﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Video;
using AForge.Video.DirectShow;

namespace RGB_Proje
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private FilterInfoCollection CaptureDevices;
        private VideoCaptureDevice videoSource;

        public Difference Difference { get; private set; }

        private void Form1_Load(object sender, EventArgs e)
        {
            CaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo Device in CaptureDevices)
            {
                comboBox1.Items.Add(Device.Name);
            }
            comboBox1.SelectedIndex = 0;
            videoSource = new VideoCaptureDevice();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            videoSource = new VideoCaptureDevice(CaptureDevices[comboBox1.SelectedIndex].MonikerString);
            videoSource.NewFrame += new NewFrameEventHandler(VideoSource_NewFrame);
            videoSource.Start();
        }

        private void VideoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap image = (Bitmap)eventArgs.Frame.Clone();
            Bitmap image1 = (Bitmap)eventArgs.Frame.Clone();
            Bitmap image2 = (Bitmap)eventArgs.Frame.Clone();

        
            pictureBox1.Image = image;

            // Channel filtresi
            ChannelFiltering filter = new ChannelFiltering();
            // kırmızı renk için
            filter.Red = new IntRange(200, 255);
            filter.Green = new IntRange(254, 255);
            filter.Blue = new IntRange(254,255);
            // yeşil renk için
            filter.ApplyInPlace(image1);
            filter.Red = new IntRange(254, 255);
            filter.Green = new IntRange(200, 255);
            filter.Blue = new IntRange(254, 255);
            // mavi renk için
            filter.ApplyInPlace(image2);
            filter.Red = new IntRange(254, 255);
            filter.Green = new IntRange(254, 255);
            filter.Blue = new IntRange(200,255);
            // filtrenin uygulanması
            filter.ApplyInPlace(image2);
            // Birleştirme Filtresi
            Merge birleştir = new Merge(image2);
            // apply the filter
            Bitmap resultImage = filter.Apply(image1);

            //Filtre uygulandıktan sonra resmin sonucu
            pictureBox2.Image = resultImage;

        }
        private void button4_Click(object sender, EventArgs e)
        {
            pictureBox2.Image = (Bitmap)pictureBox1.Image.Clone();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (videoSource.IsRunning == true)
            {
                videoSource.Stop();
            }
            Application.Exit(null);
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
